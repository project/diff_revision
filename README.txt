-- SUMMARY --

This module provides features show the difference of revisions nodes with help the daisydiff library.
You may choose some the mode work:
1 - Always show difference
2 - Show difference "n" days after publications
3 - Show difference for user once, if this is marked "new"
 
Demo - http://

-- REQUIREMENTS --

-- INSTALLATION --

* Tick module on page modules

-- CONFIGURATION --

* Configure user permissions in Administer >> User management >> Permissions

* Customize the menu settings in Administer >> Site configuration >> Diff revision

-- USE --

1. Create the node.
2. Create the revision for this node.

-- CONTACT --

Developer http://drupal.org/user/203339
This project has been sponsored by:
* JASMiND
  Is an independent professional consulting group with extensive background in telecommunications, IT, software development and implementation. Visit http://jmproduct.ru/en for more information.